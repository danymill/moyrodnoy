<?php
/**
 * @file
 * view_catalog_terms.features.inc
 */

/**
 * Implements hook_views_api().
 */
function view_catalog_terms_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
