<?php
/**
 * @file
 * views_brands_page.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_brands_page_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'brands_page';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Страница брендов';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Бренды товаров';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'ещё';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '60';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Элементов на страницу';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Все -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Пропустить';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« первая';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ предыдущая';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'следующая ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'последняя »';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Связь: Термин таксономии: Содержимое, используя Бренд */
  $handler->display->display_options['relationships']['reverse_field_product_brand_node']['id'] = 'reverse_field_product_brand_node';
  $handler->display->display_options['relationships']['reverse_field_product_brand_node']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['reverse_field_product_brand_node']['field'] = 'reverse_field_product_brand_node';
  $handler->display->display_options['relationships']['reverse_field_product_brand_node']['required'] = TRUE;
  /* Поле: Термин таксономии: Логотип бренда */
  $handler->display->display_options['fields']['field_brend_logo']['id'] = 'field_brend_logo';
  $handler->display->display_options['fields']['field_brend_logo']['table'] = 'field_data_field_brend_logo';
  $handler->display->display_options['fields']['field_brend_logo']['field'] = 'field_brend_logo';
  $handler->display->display_options['fields']['field_brend_logo']['label'] = '';
  $handler->display->display_options['fields']['field_brend_logo']['element_type'] = '0';
  $handler->display->display_options['fields']['field_brend_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_brend_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_brend_logo']['settings'] = array(
    'image_style' => 'brand',
    'image_link' => 'content',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Критерий сортировки: Термин таксономии: Имя */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  /* Критерий фильтра: Словарь таксономии: Машинное имя */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'products_brands' => 'products_brands',
  );
  /* Критерий фильтра: Термин таксономии: Опубликовано */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'termstatus';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Критерий фильтра: Термин таксономии: Логотип бренда (field_brend_logo:fid) */
  $handler->display->display_options['filters']['field_brend_logo_fid']['id'] = 'field_brend_logo_fid';
  $handler->display->display_options['filters']['field_brend_logo_fid']['table'] = 'field_data_field_brend_logo';
  $handler->display->display_options['filters']['field_brend_logo_fid']['field'] = 'field_brend_logo_fid';
  $handler->display->display_options['filters']['field_brend_logo_fid']['operator'] = 'not empty';
  /* Критерий фильтра: Содержимое: Опубликовано */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['relationship'] = 'reverse_field_product_brand_node';
  $handler->display->display_options['filters']['status_1']['value'] = '1';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'brendy-tovarov';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Бренды товаров';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['brands_page'] = array(
    t('Master'),
    t('Бренды товаров'),
    t('ещё'),
    t('Применить'),
    t('Сбросить'),
    t('Сортировать по'),
    t('По возрастанию'),
    t('По убыванию'),
    t('Элементов на страницу'),
    t('- Все -'),
    t('Пропустить'),
    t('« первая'),
    t('‹ предыдущая'),
    t('следующая ›'),
    t('последняя »'),
    t('field_product_brand'),
    t('Page'),
  );
  $export['brands_page'] = $view;

  return $export;
}
