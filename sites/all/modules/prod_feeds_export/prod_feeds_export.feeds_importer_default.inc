<?php
/**
 * @file
 * prod_feeds_export.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function prod_feeds_export_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'product';
  $feeds_importer->config = array(
    'name' => 'Product',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsCommerceProductProcessor',
      'config' => array(
        'product_type' => 'shirts',
        'author' => '1',
        'tax_rate' => TRUE,
        'mappings' => array(
          0 => array(
            'source' => 'sku',
            'target' => 'sku',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'img',
            'target' => 'field_img_product',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'size',
            'target' => 'field_size_clothing',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          4 => array(
            'source' => 'color',
            'target' => 'field_color',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          5 => array(
            'source' => 'price',
            'target' => 'commerce_price:amount',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'price_currency',
            'target' => 'commerce_price:currency_code',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'status',
            'target' => 'status',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'stock',
            'target' => 'commerce_stock',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'options1',
            'target' => 'field_options:first',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'options2',
            'target' => 'field_options:second',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'shirts',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['product'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'product_display';
  $feeds_importer->config = array(
    'name' => 'Product display',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'ID',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'link_product',
            'target' => 'field_link_product:sku',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'shirts',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['product_display'] = $feeds_importer;

  return $export;
}
