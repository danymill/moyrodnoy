(function($) {
	Drupal.behaviors.moyrodnoycheckout = {
		attach: function(context, settings) {
			$('#commerce-checkout-form-checkout', context).once('moyrodnoycheckout', function (){
				$('#edit-account').detach().insertAfter('#edit-commerce-fieldgroup-pane-group-profile-field-order-last-name');
				$('#edit-account').show();
				//$('#custom-payment-transfer label').html('<span class="form-required" title="This field is required."> *</span>');
				$('#edit-commerce-fieldgroup-pane-group-payment-transfer label').html(function(indx, oldHtml){
			    	return oldHtml + '<span class="form-required" title="This field is required."> *</span>';
			  });
				//$('#custom-payment-transfer').hide();
			});
			// $('#commerce-checkout-form-checkout').submit(function(){

			//     // далее проверяем, если выбран метод "Доставка по городу" и не заполнено поле "Адрес"


			//   if (($('#edit-commerce-fieldgroup-pane-group-payment-transfer-field-order-payer input').val().length == 0) & ($('input:radio:checked').val() == 'delivery')) {
			//        // выводим сообщение над незаполненным полем
			// 		$('#edit-commerce-fieldgroup-pane-group-payment-transfer-field-order-payer label').html('<div class="messages error"><h2 class="element-invisible">Error message</h2>"<em class="placeholder">Поле Адрес</em>" не заполнено</div> Адрес<span class="form-required" title="This field is required.">*</span>');
			// 		// и этим false отменяем отправку формы
			//         return false;
			//    }
			// });
		}
	};
})(jQuery);
