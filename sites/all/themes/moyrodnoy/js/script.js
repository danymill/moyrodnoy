(function($){
/**
* Hide some fields by default.
*/

$('#compare-products-table .prods tr td').wrapInner("<div class='wrap_in_td'></div>");


  Drupal.behaviors.hideTovarFields = {
    attach: function (context, settings) {
      $('.product-style .field-slideshow-carousel-wrapper,.product-style .views-field-body,.product-style .views-field-nothing').hide();
      $(".product-style .views-row").hover(
        function () {
          $(this).find('.field-slideshow-carousel-wrapper, .product-style .views-field-body, .views-field-nothing').show();
        },
        function () {
          $(this).find('.field-slideshow-carousel-wrapper, .product-style .views-field-body, .views-field-nothing').hide();
        }
      );
    }
  };

  Drupal.behaviors.befImageAsLink = {
    attach: function (context, settings) {
      $('.views-widget-filter-field_type_tid')
        .prependTo($('.views-widget-filter-field_type_tid')
          .closest('.views-exposed-form')
          .parent()
        );
      $('.bef-select-as-links', context).once(function () {
        $(this).find('.img-as-link')
          .unbind('click')
          .click(function (event) {
          var $wrapper = $(this).closest('.bef-select-as-links');
          var $options = $wrapper.find('select option');
          // We have to prevent the page load triggered by the links.
          event.preventDefault();
          event.stopPropagation();
          // Un select old select value.
          $wrapper.find('select option').removeAttr('selected');

          // Set the corresponding option inside the select element as selected.
          var link_text = $(this).data('name');
          console.log(link_text);
          var $selected = $options.filter(function () {
            return $(this).text() == link_text;
          });
          $selected.attr('selected', 'selected');
          console.log($selected);
          $wrapper.find('.bef-new-value').val($selected.val());
          $wrapper.find('a').removeClass('active');
          $(this).addClass('active');
          // Submit the form.
          $wrapper.closest('form').find('.views-submit-button *[type=submit]').click();
            return false;
        });
      });
    }
  };


  Drupal.behaviors.compare = {
    attach: function (context, settings) {
      $('body', context).once('out_com',
        function () {

          var vsego_kolvo = $("#compare-products-table tr.tr_title > td").size();
          // -- podval sravnenia
          if (vsego_kolvo > 3) {
            $(".view-compare-products .output_compare span.vsego").text(vsego_kolvo);
          }

          if (vsego_kolvo === 1) {
            $(".view-compare-products .output_compare").html('<span>Показан 1 товар</span>');
          }
          else if (vsego_kolvo === 2) {
            $(".view-compare-products .output_compare").html('<span>Показано 2 товара</span>');
          }
          else if (vsego_kolvo === 3) {
            $(".view-compare-products .output_compare").html('<span>Показано 3 товара</span>');
          }

          $(document).ajaxSuccess(function (event, data) {
            vsego_kolvo = $("#compare-products-table tr.tr_title > td").size();
          // -- podval sravnenia
            if (vsego_kolvo > 3) {
              $(".view-compare-products .output_compare span.vsego").text(vsego_kolvo);
            }

            if (vsego_kolvo === 1) {
              $(".view-compare-products .output_compare").html('<span>Показан 1 товар</span>');
            }
            else if (vsego_kolvo === 2) {
              $(".view-compare-products .output_compare").html('<span>Показано 2 товара</span>');
            }
            else if (vsego_kolvo === 3) {
              $(".view-compare-products .output_compare").html('<span>Показано 3 товара</span>');
            }
          });

        });

      $('#compare-products-table .prods tr td').css('min-width', '236px');
    }
  };


  Drupal.behaviors.moveFiler = {
    attach: function(context, settings) {
      $('.view-taxonomy-term .view-filters form').appendTo('.view-taxonomy-term.product-style-catalog .view-header');
    }
  };

  Drupal.behaviors.moyrodnoyScripts = {
    attach: function (context, settings) {
//    add captions to modal windows
      var reviewCaption = Drupal.t('Add new review');
      var advertCaption = Drupal.t('Add new advert');
      var loginCaption = Drupal.t('Войти');
      if ($("#modalContent #comment-form").length > 0) {
        $('.node-type-free #modalContent #edit-submit').attr("value", 'Разместить');
        $('.node-type-reviews #modalContent #modal-title').text(reviewCaption).show();
        $('.node-type-free #modalContent #modal-title').text(advertCaption).show();
        $('#modalContent').addClass('modalContent_comment_form');
      }
      else {
        $('#modalContent #modal-title').text(loginCaption).show();
      };

      if ($(".node-type-free #modalContent #comment-form").length > 0) {
        $('#modalContent').removeClass('modalContent_comment_form');
        $('#modalContent').addClass('modalContent_comment_form_free');
      }
      if ($(".node-type-reviews #modalContent #comment-form").length > 0) {
        $('#modalContent').removeClass('modalContent_comment_form');
        $('#modalContent').addClass('modalContent_comment_form_reviews');
      }

      if ($(".node-type-product #modalContent #comment-form").length > 0) {
        $('#modalContent').removeClass('modalContent_comment_form');
        $('#modalContent').addClass('modalContent_comment_form_product');
      }

      // PRODUCT SLIDESHOW
      $('.field-slideshow-carousel-wrapper .jcarousel-item a').on('hover', function () {
        $(this).click();
      });


// Sravnenie Tovarov


      // $('#compare-products-table .prods tr').each(function (index) {
      //   var trHeight = $(this).height();
      //   var trClass = $(this).attr("class");
      //   $('#compare-products-table').find('tr').eq(index).attr("class", trClass).css("height", trHeight);
      // });





      // Sravnenie Tovarov
$('#compare-products-table .prods tr').each(function(index) {
          var trHeight = $(this).height();
          var trClass = $(this).attr("class");
          var trHeightTh = Math.round($('#compare-products-table .headers').find('tr').eq(index).height());
// console.log(trHeight+' - '+trClass+' - '+trHeightTh);
        if (trHeightTh > trHeight) {
          $(this).css("height", trHeightTh)
        }
        else {
          $('#compare-products-table .headers').find('tr').eq(index).attr("class", trClass).css("height", trHeight);
        }
      });

        $('#compare-products-table .prods, #compare-products-table .headers').addClass('one_height');

        var height_cont = 0;
        $("#compare-products-table .one_height").each(function () {

            $(this).find("table").each(function () {
                if ($(this).height() > height_cont) {
                    height_cont = $(this).height();
                }
            });
        });
        $("#compare-products-table .one_height").height(height_cont);

//--prokrutka-v-sravnenii--

  var pane = $('#compare-products-table .prods');
  var td_width = $('.prods table td').outerWidth();
  pane.jScrollPane(
    {
      arrowButtonSpeed: td_width,
      showArrows: true,
      animateScroll: true
    }
  );
  var api = pane.data('jsp');



  $('.jspArrowLeft').click(
    function()
    {
      api.scrollByX(0 - td_width);
      return false;
    }
  );

    $('.jspArrowRight').click(
    function()
    {
      api.scrollByX(td_width);
      return false;
    }
  );








// PRODUCT CAROUSEL

      $('.view-id-product_page.view-display-id-block_5 .views-row .views-field-view:not(:first)').hide();
      $('.view-id-product_page.view-display-id-block_5 .views-row .views-field-name:first').addClass('active');
      $('.view-id-product_page.view-display-id-block_5 .views-row .views-field-name').click(function () {
        $('.view-id-product_page.view-display-id-block_5 .views-row .views-field-name.active').removeClass('active').next().hide();
        $(this).addClass('active').next().show();
      });
      if ($('.view-id-product_page ul.jcarousel').length > 0) {
        $('.view-id-product_page ul.jcarousel').jcarousel();
      }

    }
  };



    $(window).load(function(){

// $('#compare-products-table .prods tr td').wrapInner("<div class='wrap_in_td'></div>");
        // Sravnenie Tovarov
        // $('#compare-products-table .prods tr').each(function(index) {
        //     var trHeight = $(this).height();
        //     var trClass = $(this).attr("class");
        //     $('#compare-products-table').find('tr').eq(index).attr("class", trClass).css("height", trHeight);

        // });



      // Sravnenie Tovarov
$('#compare-products-table .prods tr').each(function(index) {
          var trHeight = $(this).height();
          var trClass = $(this).attr("class");
          var trHeightTh = Math.round($('#compare-products-table .headers').find('tr').eq(index).height());
// console.log(trHeight+' - '+trClass+' - '+trHeightTh);
        if (trHeightTh > trHeight) {
          $(this).css("height", trHeightTh)
        }
        else {
          $('#compare-products-table .headers').find('tr').eq(index).attr("class", trClass).css("height", trHeight);
        }
      });

        $('#compare-products-table .prods, #compare-products-table .headers').addClass('one_height');

        var height_cont = 0;
        $("#compare-products-table .one_height").each(function () {

            $(this).find("table").each(function () {
                if ($(this).height() > height_cont) {
                    height_cont = $(this).height();
                }
            });
        });
        $("#compare-products-table .one_height").height(height_cont);

      // var k = jQuery.noConflict();
      // function equalHeight(group) {
      //     var tallest = 0;
      //     group.each(function() {
      //         thisHeight = k(this).height();
      //         if(thisHeight > tallest) {
      //             tallest = thisHeight;
      //         }
      //     });
      //     group.height(tallest);
      // }
      // k(document).ready(function(){
      //     equalHeight(k(".genclass"));
      // });
        

  
    });

  Drupal.behaviors.copyPrevent = {
    attach: function (context, settings) {
      $('body:not(.allow-copy)').once('copyPrevent', function () {
        document.onselectstart = function (e) {
          e.preventDefault ? e.preventDefault() : e.returnValue = false;
        };

        document.oncopy = function (event) {
          var clipboardData = (event || window.event).clipboardData;
          window.getSelection().removeAllRanges();
          if (clipboardData) {
            clipboardData.setData('Text', 'Все права защищены!');
          }

          return false;
        }
      });
    }
  };

  $(document).ready(function(){

$('#compare-products-table .prods tr td').css('min-width', '236px');


//    add scroll header
        $(function(){
            $(window).scroll(function() {
                var top = $(document).scrollTop();
                if (top < 1) $('.header-fix').removeClass('tiny') && $('#slider, #main').removeClass('for-tiny');
                else $('.header-fix').addClass('tiny') && $('#slider, #main').addClass('for-tiny');
            });
        });

//    Colorbox webforms
    $('#cboxLoadedContent .node-webform > h2').remove();

//    partners
    $('.partner .more-partner a').click(function(){
        $(this).parents('.partner').toggleClass('open');
        return false;
    })

// UI TABS

    if ($('.view-id-parthners_captions').length > 0) {
      $('.view-id-parthners_captions').tabs();
    };

    $('#ui-id-custom').click(function(){
      if ($('#ui-id-2').length > 0) {
        $('#ui-id-2').click();
      };
      if ($('#quicktabs-tab-product_tabs-1').length > 0) {
        $('#quicktabs-tab-product_tabs-1').click();
      };
    });


//    Collapse text
    $('.collapse-title').next('.collapse-text').hide();
    $('.collapse-title').each(function () {
      var wrapper = $(this).parent('.collapse-wrapper');
      var text = $(this).next('.collapse-text');
      var processed = $(this).add(wrapper).add(text);
      if (text.css('display') == 'block') {
        processed.toggleClass('processed');
      }
      $(this).click(function () {
        if (text.css('display') == 'block') {
          text.slideUp(400, function () {
            processed.toggleClass('processed');
          });
        } else {
          text.slideDown(400, function () {
            processed.toggleClass('processed');
          });
        }
        return false;
      });
    });

      // var k = jQuery.noConflict();
      // function equalHeight(group) {
      //     var tallest = 0;
      //     group.each(function() {
      //         thisHeight = k(this).height();
      //         if(thisHeight > tallest) {
      //             tallest = thisHeight;
      //         }
      //     });
      //     group.height(tallest);
      // }
      // k(document).ready(function(){
      //     equalHeight(k(".genclass"));
      // });


        $('#compare-products-table .prods, #compare-products-table .headers').addClass('one_height');

        var height_cont = 0;
        $("#compare-products-table .one_height").each(function () {

            $(this).find("table").each(function () {
                if ($(this).height() > height_cont) {
                    height_cont = $(this).height();
                }
            });
        });
        $("#compare-products-table .one_height").height(height_cont);        $('#compare-products-table .prods, #compare-products-table .headers').addClass('one_height');

        var height_cont = 0;
        $("#compare-products-table .one_height").each(function () {

            $(this).find("table").each(function () {
                if ($(this).height() > height_cont) {
                    height_cont = $(this).height();
                }
            });
        });
        $("#compare-products-table .one_height").height(height_cont);

//    TB MEGAMENU
    var tb_links = $('li.level-1-1, li.level-2-1, li.level-3-1, li.level-4-1, li.level-5-1, li.level-6-1, li.level-7-1').each(function() {
      $(this).addClass('processed');
    });

    var tb_blocks = $('div.level-1-1, div.level-2-1, div.level-3-1, div.level-4-1, div.level-5-1, div.level-6-1, div.level-7-1').each(function() {
      $(this).addClass('processed');
    });

    $('.tb-megamenu-subnav > li').each(
      function(){
        $(this).hover(function () {
            if ($(this).hasClass('processed')) {
              return false;
            }
            else {
              var tb_elem = $(this);
              var tb_parent = $(this).closest('.mega-dropdown-inner').addClass('activeInner');
              $('.tb-megamenu-subnav  li', tb_parent).removeClass('processed');
              $(this).addClass('processed');
              linkklass = $(this).attr('data-class');
              if(linkklass) {
                tb_parent.find('div.processed').not($('div.' + linkklass)).toggleClass('processed').stop().hide();
                $('div.' + linkklass).toggleClass('processed').stop().show();
              }
            }
          },

          function () {
            return;
          });
      }
    );

    
    
  });




})(jQuery);



