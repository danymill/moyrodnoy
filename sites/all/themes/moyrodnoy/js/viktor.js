(function($){
    $(document).ready(function(){

        // add "parent" class in #block-menu-menu-products


        $('.menu-sidebar-catalog > .view-content > .item-list > ul > li > div > ul > li > .views-field-name a.active').closest('li').addClass('active-parent')

        $('.menu-sidebar-catalog > .view-content > .item-list > ul > li > div > ul > li > .views-field-name').closest('li').addClass('item-menu-sidebar-catalog')

        $('.menu-sidebar-catalog .item-list a.active').closest('.item-menu-sidebar-catalog').addClass('active-parent')


        // expand and collapse categoies menues

        $('.item-menu-sidebar-catalog .item-list ul').hide();

        $('.item-menu-sidebar-catalog.active-parent ul').show();
//        $("button[disabled]").next().text("this button is disabled").show();
//        $("p").next(".selected").show();

        $('.item-menu-sidebar-catalog .item-list ul').each(function() {

            if($(this).css('display') == 'block') {

                $(this).parent().addClass('xpndd')

            }

            $(this).prev().prev().addClass('collapsible').click(function() {

                if ($(this).next().next().css('display') == 'block') {

                    $(this).next().next().slideUp(200, function () {

                        $(this).prev().prev().removeClass('expanded').addClass('collapsed').parent().removeClass('xpndd').addClass('cllpsd');

                    });

                } else {

                    $(this).next().next().slideDown(200, function () {

                        $(this).prev().prev().removeClass('collapsed').addClass('expanded').parent().removeClass('cllpsd').addClass('xpndd');

                    });

                }

                return false;

            });

        });


        // call-popup visible when scroll down
        $(window).scroll(function () {
            if ($(this).scrollTop() > 300) $('.skip-top').fadeIn();
            else $('.skip-top').fadeOut(400);
        });

        $('a#skip-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 700);
            return false;
        });


    })


})(jQuery);