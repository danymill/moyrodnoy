<?php

/**
 * @file
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $created: Formatted date and time for when the comment was created.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->created variable.
 * - $changed: Formatted date and time for when the comment was last changed.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->changed variable.
 * - $new: New comment marker.
 * - $permalink: Comment permalink.
 * - $submitted: Submission information created from $author and $created during
 *   template_preprocess_comment().
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $title: Linked title.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - comment: The current template type, i.e., "theming hook".
 *   - comment-by-anonymous: Comment by an unregistered user.
 *   - comment-by-node-author: Comment by the author of the parent node.
 *   - comment-preview: When previewing a new or edited comment.
 *   The following applies only to viewers who are registered users:
 *   - comment-unpublished: An unpublished comment visible only to administrators.
 *   - comment-by-viewer: Comment by the user currently viewing the page.
 *   - comment-new: New comment since last the visit.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * These two variables are provided for context:
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_comment()
 * @see template_process()
 * @see theme_comment()
 *
 * @ingroup themeable
 */
unset($content['links']['comment']['#links']['comment-reply']);
global $user;
?>
<div class="<?php print $classes; ?> advert clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>

  <?php print render($title_suffix); ?>

  <div class="comment-head clearfix">
    <div class="comment-date">
      <?php print format_date($comment->created, 'custom', 'd.m.Y' . t(' \i\n ') . 'H:i'); ?>
    </div>
    <div class="comment-author darom-autor">
      <?php if (isset($content['field_locality'])): ?>
        <span class="comment-author-wrapper"><?php print $author; ?></span>, <span
            class="comment-author-city"><?php print render($content['field_locality']['#items'][0]['value']); ?></span>
        <?php unset($content['field_locality']); ?>
      <?php else: ?>
        <span class="comment-author-wrapper"><?php print $author; ?></span>
      <?php endif; ?>
    </div>
  </div>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <div class="comment-author-img darom-img">
      <?php print render($content['field_darom_img']); ?>
      <?php if(isset($new)): ?>
      <span class="new new-review"><?php print $new ?></span>
      <?php endif; ?>
    </div>
    <div class="comment-content">
      <div class="comment-subject">
        <?php print $comment->subject; ?>
      </div>
      <?php
      hide($content['links']);
      print render($content);
      ?>
    </div>
  </div>
    <?php if (user_is_logged_in()): ?>
    <div class="comment-links-wrapper clearfix">
     <?php print render($content['links']); ?>
    </div>
    <?php endif; ?>
</div>
