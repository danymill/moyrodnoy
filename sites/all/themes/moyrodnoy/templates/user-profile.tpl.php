<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>
<?php if (user_is_logged_in()): ?>
<?php
if (arg(0) == 'user' && is_numeric(arg(1))) {
	$user_fields = user_load(arg(1));
}
else{
	global $user;
	$user_fields = user_load($user->uid);
}

$fio = '';
if (isset($user_fields->field_user_surname['und'][0]['safe_value'])) {
		$fio .= $user_fields->field_user_surname['und'][0]['safe_value'];
}
if (isset($user_fields->field_user_name['und'][0]['safe_value'])) {
		$fio .= ' '.$user_fields->field_user_name['und'][0]['safe_value'];
}
if (isset($user_fields->field_user_last_name['und'][0]['safe_value'])) {
		$fio .= ' '.$user_fields->field_user_last_name['und'][0]['safe_value'];
}

$phone = '';
if (!empty($user_fields->field_user_phone)) {
	$phone = $user_fields->field_user_phone['und'][0]['safe_value'];
}

if (!empty($user_fields->field_user_city)) {
	$city_name = $user_fields->field_user_city['und'][0]['safe_value'];
	$first = mb_substr($city_name,0,1, 'UTF-8');//первая буква
	$last = mb_substr($city_name,1);//все кроме первой буквы
	$first = mb_strtoupper($first, 'UTF-8');
	$last = mb_strtolower($last, 'UTF-8');
	$city_name1 = $first.$last;
	$city = 'г.'.$city_name1;
}

$i = 1;
// dpm($user_profile['user_picture']);
//print theme('image_style', array( 'path' =>  $user_fields->picture->uri, 'style_name' => 'user_img', 'width' => '190', 'height' => '190'));
?>

<div class="profile"<?php print $attributes; ?>>
	<div class="user_img">
		<?php if (isset($user_fields->picture->uri)): ?>
			<img src="<?php print image_style_url('user_img', $user_fields->picture->uri); ?>" />
		<?php endif ?>
	</div>
	<div id="details">
		<a href="/user/<?php print $user_fields->uid; ?>/edit" class="edit_profile" title=""><span>Редактировать</span></a>
		<h1 class="page-title">Личный кабинет</h1>
		<div class="profil_label">ПЕРСОНАЛЬНЫЕ ДАННЫЕ:</div>
		<div class="field  field-label-inline clearfix"><div class="field-label">ФИО:&nbsp;</div><div class="field-items"><div class="field-item"><?php print render($fio); ?></div></div></div>
		<div class="field  field-label-inline clearfix"><div class="field-label">Email:&nbsp;</div><div class="field-items"><div class="field-item"><?php print render($user_fields->mail); ?></div></div></div>
		<?php if (!empty($user_fields->field_user_phone)): ?>
			<div class="field  field-label-inline clearfix"><div class="field-label">Тел:&nbsp;</div><div class="field-items"><div class="field-item"><?php print render($phone); ?></div></div></div>
		<?php endif ?>
		<?php if (!empty($user_fields->field_user_city)): ?>
			<div class="field  field-label-inline clearfix"><div class="field-label">Город:&nbsp;</div><div class="field-items"><div class="field-item"><?php print render($city); ?></div></div></div>
		<?php endif ?>
		<?php if (isset($user_fields->field_address_shipping['und'])): ?>
			<?php foreach ($user_fields->field_address_shipping['und'] as $key => $value): ?>
				<div class="field  field-label-inline clearfix"><div class="field-label">Адрес доставки<?php if($i > 1) print(' '.$i) ?>:&nbsp;</div><div class="field-items"><div class="field-item"><?php print render($value['safe_value']); ?></div></div></div>
				<?php $i++; ?>
			<?php endforeach ?>
		<?php endif ?>
		<div class="message" style="display: none;">
			<p>Спасибо за регистрацию в интернет-магазине MoyRodnoy.by! <br>
				Ваша учетная запись успешно создана.<br>
				Теперь Вы сможете пользоваться услугами, доступными только для наших клиентов: следить за изменением статуса заказа в режиме онлайн, операвтино получать информацию о выгодных Акциях, интересных конкурсах, хороших скидках.</p>
				<ul>
					<li>В разделе «Заказы» Вы сможете отслеживать статус Ваших заказов.</li>
					<li>В разделе «Отложенные товары» Вы сможете найти отложенные Вами товары, которые Вы хотите приобрести позднее.</li>
					<li>Если у Вас поменялся адрес доставки или телефон, то Вы всегда сможете изменить персональные данные в разделе «Мои данные».</li>
				</ul>
		</div>
	</div>
</div>
<?php if (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == ''): ?>
<?php
$block = block_load('block', '55');
$block_content = _block_render_blocks(array($block));
$build = _block_get_renderable_array($block_content);
print drupal_render($build);
 ?>
<?php endif ?>
<?php endif ?>


