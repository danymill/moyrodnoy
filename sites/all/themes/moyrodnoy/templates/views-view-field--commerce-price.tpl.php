<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
$output= '';
if (isset($row->field_commerce_price[0]['raw']['data']['components'][1]['price']['amount'])) {
    $discont = $row->field_commerce_price[0]['raw']['amount'];
//    $discont = commerce_currency_format($row->field_commerce_price[0]['raw']['amount'], $row->field_commerce_price[0]['raw']['currency_code']);
    $output .= '<div class="commerce-price-discont">' . number_format($discont / 100, 2, ',', ' ') . ' руб.</div>';
}
if (isset($row->field_commerce_price[0]['raw']['data']['components'][0]['price']['amount'])) {
    $price = $row->field_commerce_price[0]['raw']['original']['amount'];
//  $price = commerce_currency_format($row->field_commerce_price[0]['raw']['original']['amount'], $row->field_commerce_price[0]['raw']['currency_code']);
    $output .= '<div class="commerce-price">' . number_format($price / 100, 2, ',', ' ') . ' руб.</div>';
}
?>
<?php print $output; ?>
