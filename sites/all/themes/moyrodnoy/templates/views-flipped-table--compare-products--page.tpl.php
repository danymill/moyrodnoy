<?php
foreach($view->result as $delta => $result){
    $double_fields[$delta] = $view->result[$delta]->field_field_product_options;
}
foreach($double_fields[0] as $delta=> $item){
    $header_double[] = $item['raw']['first'];
}
?>
<div id="compare-products-table" class="clearfix">
    <div class="headers genclass">
      <table>
        <tbody>
        <?php foreach ($rows_flipped as $field_name => $row) : ?>
        <tr class="<?php print $row_classes_flipped[$field_name] . '&nbsp; tr_' . $field_name; ?>">
          <th>
              <?php
              if(!empty($header[$field_name])) echo $header[$field_name];
              ?>
          </th>
        </tr>
        <?php
        $current_class = $row_classes_flipped[$field_name];
        endforeach; ?>
        <?php foreach ($header_double as $header) : ?>
            <tr class="<?php print $current_class == 'odd' ? 'even' : 'odd'; ?>">
                <?php $current_class = $current_class == 'odd' ? 'even' : 'odd'; ?>
                <th>
                    <?php echo $header; ?>
                </th>
            </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="prods genclass">
      <table>
        <tbody>
        <?php foreach ($rows_flipped as $field_name => $row) : ?>
        <tr class="<?php print $row_classes_flipped[$field_name] . '&nbsp; tr_' . $field_name; ?>">
          <?php foreach ($row as $index => $item): ?>
          <td>
            <?php echo $item; ?>
          </td>
          <?php endforeach; ?>

        </tr>
          <?php endforeach; ?>

        <?php foreach ($header_double as $delta => $value) : ?>


            <tr class="<?php print $current_class == 'perv' ? 'vtor' : 'perv'; ?>">
                <?php $current_class = $current_class == 'perv' ? 'vtor' : 'perv'; ?>
            <?php foreach ($double_fields as $num => $items): ?>
                <td>
                    <?php echo $items[$delta]['raw']['second'] ?>
                </td>
            <?php endforeach; ?>

        </tr>
        <?php endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>