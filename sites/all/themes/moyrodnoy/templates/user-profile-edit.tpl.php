

<?php

	$user_fields = user_load(arg(1));

	print render($form['form_id']);

	print render($form['form_build_id']);

	print render($form['form_token']);

	//print drupal_render_children($form);
// dpm($form);
?>

<div class="profile">

	<div class="user_img">

		<?php print render($form['picture']); ?>

	</div>

	<div id="details">

		<h1 class="page-title">Личный кабинет</h1>

		<div class="profil_label">ПЕРСОНАЛЬНЫЕ ДАННЫЕ:</div>

		<div class="bl_left">

			<?php

		  print render($form['field_user_surname']);

		  print render($form['field_user_name']);

		  print render($form['field_user_last_name']);

		  print render($form['account']['mail']);

		  print render($form['field_user_phone']);

		 ?>

		</div>

		<div class="bl_right">

			<?php

				print render($form['account']['current_pass']);

				print render($form['account']['pass']);

			?>

		</div>

		<div class="bl_btm">

			<?php

				print render($form['field_user_city']);

				print render($form['field_address_shipping']);
				
				print render($form['field_subscription']);

			?>

		</div>

	</div>

	<div class="submit_wrap">

		<!-- <input type="submit" name="op" id="edit-submit" value="Save" /> -->

		<?php print drupal_render_children($form['actions']); ?>

	</div>

</div>

<?php if (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == 'edit'): ?>

<?php

$block = block_load('block', '55');

$block_content = _block_render_blocks(array($block));

$build = _block_get_renderable_array($block_content);

print drupal_render($build);

 ?>

<?php endif ?>

