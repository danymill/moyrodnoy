<?php

function moyrodnoy_theme($existing, $type, $theme, $path){
  return array(
    'user_register_form' => array( // название хука темизации
      'render element'=>'form',
      'template' =>'templates/user-register',
    ),
    'commerce_checkout_form_checkout' => array(
      'render element' => 'form',
      'template' => 'commerce-checkout-form-checkout',
      'path' => drupal_get_path('theme', 'moyrodnoy') . '/templates',
    ),
    // 'blog_node_form' => array( // название хука темизации
    //   'arguments' => array('form' => NULL),
    //   'render element' => 'form',
    //   'template' =>'templates/blog-add',
    // ),
    // The form ID.
    'user_profile_form' => array(
      // Forms always take the form argument.
      'arguments' => array('form' => NULL),
      'render element' => 'form',
      'template' => 'templates/user-profile-edit',
    ),
  );
}

function moyrodnoy_preprocess_user_register(&$variables) {
  $variables['form'] = drupal_build_form('user_register_form', user_register_form(array()));
}

function moyrodnoy_preprocess_commerce_checkout_form_checkout(&$vars){
  $vars['form_checkout'] = drupal_render_children($vars['form']);
}

function moyrodnoy_preprocess_node (&$node) {

  if($node['type'] == 'reviews') {
    if(isset($node['content']['links']['comment']['#links']['comment-add'])) {
      global $user;
      if ($user->uid == 1 || in_array('administrator', $user->roles) || in_array('Редактор сайта', $user->roles)) {
        unset($node['content']['links']['comment']['#links']['comment-add']);
      }
      else {
        $node['content']['links']['comment']['#links']['comment-add']['title'] = t('Add new review');
      }
    }
  }
  if($node['type'] == 'free') {
    if(isset($node['content']['links']['comment']['#links']['comment-add'])) {
      $node['content']['links']['comment']['#links']['comment-add']['title'] = t('Add new advert');
    }
  }
}
function moyrodnoy_form_blog_node_form_alter (&$form, &$form_state, $form_id) {
  $form['#validate'][] = 'moyrodnoy_blog_validate';
}

function moyrodnoy_blog_validate ($form, &$form_state) {
  $text = drupal_html_to_text($form_state['values']['body']['und'][0]['value'], $allowed_tags = NULL);
  if (strlen($text) < 1000) {
    form_set_error('body', t('Text can not be shorter than 1000 characters'));
  }
}
function moyrodnoy_node_view_alter(&$build) {

  if (user_is_anonymous()) {
    if (isset($build['links']['comment']['#links']['comment_forbidden'])) {
      //$node = node_load($build['body']['#object']->nid);
      $login_link = '<a class="ctools-use-modal ctools-modal-modal-popup-small" href="modal_forms/nojs/login">Войдите</a> или <a href="user/register" target="_blank" >зарегистрируйтесь</a>, чтобы';

      if ($build['#node']->type == 'reviews') {
        $login_link .= ' добавить отзыв';
      }
      elseif ($build['#node']->type == 'free') {
        $login_link .= ' отдать даром';
      }
      else {
        $login_link .= ' отправлять комментарии';
      }
      $build['links']['comment']['#links']['comment_forbidden']['title'] = $login_link;
    }
  }
}

/**
 * Preprocess variables for commerce-cart-block.tpl.php
 */
function moyrodnoy_preprocess_commerce_cart_block(&$vars) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $vars['order']);
  $quantity = commerce_line_items_quantity($order_wrapper->commerce_line_items, commerce_product_line_item_types());
  //$quantity_text = format_plural($quantity, '1 item', '@count items', array(), array('context' => 'product count on a Commerce order'));
  $total = $order_wrapper->commerce_order_total->value();
  $total_text = commerce_currency_format($total['amount'], $total['currency_code']);

  $prods=array(' товар', ' товара', ' товаров');
  $numberLast=intval(substr(strval($quantity),-1,1));
  $numberPreLast=intval(substr(strval($quantity),-2,2));

  if(($numberLast==1) and ($numberPreLast!=11)){
    $type=0;
  }elseif(($numberLast==0) or ((5<=$numberLast) and ($numberLast<=9)) or((11<=$numberPreLast) and ($numberPreLast<=19))){
    $type=2;
  }elseif((2<=$numberLast) and ($numberLast<=4) and ($numberLast<=4)){
    $type=1;
  }

  //$vars['contents_view'] = t('@quantity for @amount', array('@quantity' => $quantity_text, '@amount' => $total_text)) . '<br />';
  $vars['contents_view'] = '<div class="wrap-link-cart-head">' .t('В корзине <a class="link-cart-head" href="/cart">@quantity</a>', array('@quantity' => $quantity)).$prods[$type] . '</div>';
  //$vars['contents_view'] .= '<span class="left">'.l(t('Отложено мною товаров @quantity ', array('@quantity' => $quantity)), 'cart').'</span>';
  $vars['contents_view'] .= '<span class="right">'.l(t(' Оформить покупку'), 'checkout').'</span>';
}


function moyrodnoy_commerce_cart_empty_block() {
  return '<div class="cart-empty-block">' . t('В корзине ') .'<a class="link-cart-head" href="/cart">0</a>'. t(' товаров') .'</div>';
}


function moyrodnoy_form_comment_node_free_form_alter(&$form, &$form_state) {
    $form['submit']['#value'] = t("Save and add new");
}

/**
 * Implements hook_form_alter().
 */
function moyrodnoy_form_alter(&$form, &$form_state, $form_id) {
//   dpm($form);

  if ($form['#form_id'] == 'webform_client_form_140') {
    $form['submitted']['operator']['#type'] = 'select';
    $nid = node_load($_GET['nid']);
    $pid = commerce_product_load($nid->field_link_product['und'][0]['product_id']);
    $form['submitted']['artikul_tovara']['#value'] = $pid->sku;
    $form['submitted']['nazvanie_tovara']['#value'] = $pid->title;
  }
  if ($form['#id'] == 'user-register-form') {
    $form['account']['name']['#title'] = t('Логин');
    $form['field_user_address']['#sorted'] = FALSE;
  }

  if ($form['#form_id'] == 'commerce_checkout_form_checkout') {
    $form['commerce_shipping']['shipping_service']['#options']['courier_belarus'] = ' Курьером по РБ: рассчитывается индивидуально';
    $form['commerce_shipping']['shipping_service']['#options']['delivery'] = 'Наложенным платежом: рассчитывается индивидуально';
    $form['commerce_shipping']['shipping_service']['#options']['courier'] = ' Курьером: рассчитывается индивидуально';
    $form['commerce_shipping']['shipping_service']['#options']['pickup'] = 'Самовывоз: рассчитывается индивидуально';

    global $user;
    $uid = user_load($user->uid);
    $form['commerce_fieldgroup_pane__group_payment_transfer']['#states'] = array(
       'invisible'=>array(// скрываем поле если самовывоз,доставка по городу
      ':input[name="commerce_shipping[shipping_service]"]'=>array(array('value' =>'courier'),array('value' =>'pickup'),array('value' =>'courier_belarus'),),),
        'visible'=>array( // показываем если доставка почтой
      ':input[name="commerce_shipping[shipping_service]"]'=>array('value' =>'delivery'),),
    );

    //$form['buttons']['continue']['#validate'][] = 'checkout_confirm_form_validate';

    $form['account']['#type'] = 'container';
    $form['commerce_fieldgroup_pane__group_profile']['#type'] = 'container';

    $form['#attached']['js'][] = path_to_theme(). '/scripts/checkout.js';
    $form['commerce_fieldgroup_pane__group_profile']['#pre_render'] = array();
    array_unshift($form['commerce_fieldgroup_pane__group_profile']['#pre_render'], 'pre_render_group_address_information');


    if (user_is_logged_in()) {
      $options = array();
      if (isset($uid->field_address_shipping['und'])) {
        foreach ($uid->field_address_shipping['und'] as $value) {
          $options[$value['safe_value']] = $value['safe_value'];
        }
      }

      $form['commerce_fieldgroup_pane__group_profile']['field_order_name']['und'][0]['value']['#default_value'] = $uid->field_user_name['und'][0]['safe_value'];
      $form['commerce_fieldgroup_pane__group_profile']['field_order_surname']['und'][0]['value']['#default_value'] = $uid->field_user_surname['und'][0]['safe_value'];
      if (!empty($uid->field_user_last_name)) {
        $form['commerce_fieldgroup_pane__group_profile']['field_order_last_name']['und'][0]['value']['#default_value'] = $uid->field_user_last_name['und'][0]['safe_value'];
      }
      if (!empty($uid->field_user_phone)) {
        $form['commerce_fieldgroup_pane__group_profile']['field_order_phone']['und'][0]['value']['#default_value'] = $uid->field_user_phone['und'][0]['safe_value'];
        //$form['commerce_fieldgroup_pane__group_profile']['field_order_phone']['und'][0]['second']['#default_value'] = $uid->field_user_phone['und'][0]['second'];
      }
      if (!empty($uid->field_user_city)) {
        $form['commerce_fieldgroup_pane__group_profile']['field_order_city']['und'][0]['value']['#default_value'] = $uid->field_user_city['und'][0]['safe_value'];
      }
      //$form['commerce_fieldgroup_pane__group_profile']['field_order_e_mail']['und'][0]['value']['#default_value'] = $uid->mail;
      if (!empty($uid->field_address_shipping)) {
        $form['commerce_fieldgroup_pane__group_profile']['field_order_address_select']['und']['#default_value'] = $uid->field_address_shipping['und'][0]['safe_value'];
        $form['commerce_fieldgroup_pane__group_profile']['field_order_address_select']['und']['#options'] = $options;
        $form['commerce_fieldgroup_pane__group_profile']['field_order_address_select']['und']['#required'] = TRUE;
      } else {
        $form['commerce_fieldgroup_pane__group_profile']['field_order_address_text']['und'][0]['value']['#required'] = TRUE;
      }
    } else {
      $form['commerce_fieldgroup_pane__group_profile']['field_order_address_text']['und'][0]['value']['#required'] = TRUE;
    }
  }


    if($form['#id'] == 'views-exposed-form-brand-pages-page') {

        $tid = arg(2);
        $query = db_select('node', 'n');
        $query->condition('n.type', 'product');
        $query->innerJoin('field_data_field_product_brand', 'b', 'n.nid = b.entity_id');
        $query->innerJoin('field_data_field_product_category', 'k', 'n.nid = k.entity_id');
        $query->innerJoin('taxonomy_term_data', 't', 'k.field_product_category_tid = t.tid');

        $query->condition('b.field_product_brand_tid', $tid);
        $query->fields('t', array('tid'));
        $query->fields('t', array('name'));
        $objects = $query->execute()->fetchAllKeyed();

        if(count($objects) > 0) {
            $form['category']['#options'] = array('All' => '- Не указано -') + $objects;
        }
        else {
            unset($form['category']);
            unset($form['#info']['filter-category']);
        }
    }
}

function pre_render_group_address_information($form) {
  global $user;
  $uid = user_load($user->uid);
  if (user_is_logged_in() && !empty($uid->field_address_shipping)) {
    hide($form['field_order_address_text']);
  } else {
    hide($form['field_order_address_select']);
  }
  return $form;
}

/**
 * Implements hook_preprocess_page().
 */
function moyrodnoy_preprocess_page(&$variables) {
    if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
        $tid = taxonomy_term_load(arg(2));
        if ($tid->vocabulary_machine_name == 'categories') {
            $variables['title'] = $tid->name;
        }
    }
}

function moyrodnoy_form_views_exposed_form_alter(&$form, &$form_state){
    if ($form['#id'] == 'views-exposed-form-commerce-search-default') {
        $form['search_api_views_fulltext']['#attributes']["placeholder"] = t('Поиск по товарам');
    }
    if ($form['#id'] == 'views-exposed-form-search-node-default') {
        $form['search_api_views_fulltext']['#attributes']["placeholder"] = t('Поиск по блогу');
    }
}


/**
 * Implements hook_preprocess_html().
 */
function moyrodnoy_preprocess_html(&$variables) {
 /*add class to body for logged in admins*/
  global $user;
  if (in_array('administrator', $user->roles) || in_array('Редактор сайта', $user->roles) || in_array('Админ', $user->roles)) {
    $variables['classes_array'][] = 'allow-copy';
  }
}

function moyrodnoy_preprocess_views_exposed_form(&$vars) {
//  dpm($vars);
}


/**
 * Themes a select drop-down as a collection of links.
 *
 * @see http://api.drupal.org/api/function/theme_select/7
 *
 * @param array $vars
 *   An array of arrays, the 'element' item holds the properties of the element.
 *
 * @return string
 *   HTML representing the form element.
 */
function moyrodnoy_select_as_links($vars) {
  $element = $vars['element'];

  $output = '';
  $name = $element['#name'];

  /*Left in options only tids which have connected nodes*/
  if ($name == 'field_type_tid') {
    $current_term = menu_get_object('taxonomy_term', 2);
    $current_term_nodes = _moyrodnoy_taxonomy_get_nodes($current_term->tid, TRUE, 'field_product_category'); //Get all NIDs of current category page
    foreach ($element['#options'] as $key => $value) {
      if (is_numeric($key) && !count(array_intersect(_moyrodnoy_taxonomy_get_nodes($key, TRUE, 'field_type'), $current_term_nodes))) {
        unset($vars['element']['#options'][$key]);
        unset($element['#options'][$key]);
      }
    }
    if (count($element['#options']) < 2) {
      $vars['element']['#options'] = array();
      $element['#options'] = array();
    }
  }
  if ($name != 'product_type') {
    // Collect selected values so we can properly style the links later.
    $selected_options = array();
    if (empty($element['#value'])) {
      if (!empty($element['#default_values'])) {
        $selected_options[] = $element['#default_values'];
      }
    }
    else {
      $selected_options[] = $element['#value'];
    }

    // Add to the selected options specified by Views whatever options are in the
    // URL query string, but only for this filter.
    $urllist = parse_url(request_uri());
    if (isset($urllist['query'])) {
      $query = array();
      parse_str(urldecode($urllist['query']), $query);
      foreach ($query as $key => $value) {
        if ($key != $name) {
          continue;
        }
        if (is_array($value)) {
          // This filter allows multiple selections, so put each one on the
          // selected_options array.
          foreach ($value as $option) {
            $selected_options[] = $option;
          }
        }
        else {
          $selected_options[] = $value;
        }
      }
    }

    // Clean incoming values to prevent XSS attacks.
    if (is_array($element['#value'])) {
      foreach ($element['#value'] as $index => $item) {
        unset($element['#value'][$index]);
        $element['#value'][check_plain($index)] = check_plain($item);
      }
    }
    elseif (is_string($element['#value'])) {
      $element['#value'] = check_plain($element['#value']);
    }

    // Go through each filter option and build the appropriate link or plain text.
    foreach ($element['#options'] as $option => $elem) {
      if (!empty($element['#hidden_options'][$option])) {
        continue;
      }
      // Check for Taxonomy-based filters.
      if (is_object($elem)) {
        $slice = array_slice($elem->option, 0, 1, TRUE);
        list($option, $elem) = each($slice);
      }

      // Check for optgroups.  Put subelements in the $element_set array and add
      // a group heading. Otherwise, just add the element to the set.
      $element_set = array();
      if (is_array($elem)) {
        $element_set = $elem;
      }
      else {
        $element_set[$option] = $elem;
      }

      $links = array();
      $multiple = !empty($element['#multiple']);

      // If we're in an exposed block, we'll get passed a path to use for the
      // Views results page.
      $path = '';
      if (!empty($element['#bef_path'])) {
        $path = $element['#bef_path'];
      }

      foreach ($element_set as $key => $value) {
        $element_output = '';
        // Custom ID for each link based on the <select>'s original ID.
        $id = drupal_html_id($element['#id'] . '-' . $key);
        $elem = array(
          '#id' => $id,
          '#markup' => '',
          '#type' => 'bef-link',
          '#name' => $id,
        );

        $link_options = array();
        // Add "active" class to the currently active filter link.
        if (in_array((string) $key, $selected_options)) {
          $link_options['attributes']['class'][] = 'active';
        }
        $url = bef_replace_query_string_arg($name, $key, $multiple, FALSE, $path);
        $img = '';
        if ($name == 'field_type_tid') {
          if ($key == 'All') {
            $scheme = file_default_scheme();
            $image_uri = $scheme . '://product-type/pokazat_vse.jpg';
            $src = file_create_url($image_uri);
            $img = '<img src="' . $src . '" alt="' . $key . '" />';
          }
          else {
            $term = taxonomy_term_load($key);
            if ($term) {
              $field = field_view_field('taxonomy_term', $term, 'field_type_icon');
              if ($field) {
                $img = drupal_render($field[0]);
              }
            }
          }
          if ($img) {
            $link_options['html'] = TRUE;
            $link_options['attributes']['data-name'] = $value;
            $link_options['attributes']['class'][] = 'img-as-link';
            $value = $img;
          }
        }
        $elem['#children'] = l($value, $url, $link_options);
        $element_output = theme('form_element', array('element' => $elem));

        if (!empty($element['#settings']['combine_param']) && $element['#name'] == $element['#settings']['combine_param'] && !empty($element['#settings']['toggle_links'])) {
          $sort_pair = explode(' ', $key);
          if (count($sort_pair) == 2) {
            // Highlight the link if it is the selected sort_by (can be either
            // asc or desc, it doesn't matter).
            if (strpos($selected_options[0], $sort_pair[0]) === 0) {
              $element_output = str_replace('form-item', 'form-item selected', $element_output);
            }
          }
        }
        $output .= $element_output;

      }
    }

    $properties = array(
      '#description' => isset($element['#bef_description']) ? $element['#bef_description'] : '',
      '#children' => $output,
    );

    $output = '<div class="bef-select-as-links">';
    $output .= theme('form_element', array('element' => $properties));

//    dpm($vars['element']);
    // Add attribute that hides the select form element.
    $vars['element']['#attributes']['style'] = 'display: none;';
    $output .= theme('select', array('element' => $vars['element']));
    if (!empty($element['#value'])) {
      if (is_array($element['#value'])) {
        foreach ($element['#value'] as $value) {
          $output .= '<input type="hidden" class="bef-new-value" name="' . $name . '[]" value="' . $value . '" />';
        }
      }
      else {
        $output .= '<input type="hidden" class="bef-new-value" name="' . $name . '" value="' . $element['#value'] . '" />';
      }
    }
    $output .= '</div>';
  }

  else {
//    dpm('check this out');
//    dpm($vars);
    // Collect selected values so we can properly style the links later.
    $selected_options = array();
    if (empty($element['#value'])) {
      if (!empty($element['#default_values'])) {
        $selected_options[] = $element['#default_values'];
      }
    }
    else {
      $selected_options[] = $element['#value'];
    }

    // Add to the selected options specified by Views whatever options are in the
    // URL query string, but only for this filter.
    $urllist = parse_url(request_uri());
    if (isset($urllist['query'])) {
      $query = array();
      parse_str(urldecode($urllist['query']), $query);
      foreach ($query as $key => $value) {
        if ($key != $name) {
          continue;
        }
        if (is_array($value)) {
          // This filter allows multiple selections, so put each one on the
          // selected_options array.
          foreach ($value as $option) {
            $selected_options[] = $option;
          }
        }
        else {
          $selected_options[] = $value;
        }
      }
    }

    // Clean incoming values to prevent XSS attacks.
    if (is_array($element['#value'])) {
      foreach ($element['#value'] as $index => $item) {
        unset($element['#value'][$index]);
        $element['#value'][check_plain($index)] = check_plain($item);
      }
    }
    elseif (is_string($element['#value'])) {
      $element['#value'] = check_plain($element['#value']);
    }

    // Go through each filter option and build the appropriate link or plain text.
    foreach ($element['#options'] as $option => $elem) {
      if (!empty($element['#hidden_options'][$option])) {
        continue;
      }
      // Check for Taxonomy-based filters.
      if (is_object($elem)) {
        $slice = array_slice($elem->option, 0, 1, TRUE);
        list($option, $elem) = each($slice);
      }

      // Check for optgroups.  Put subelements in the $element_set array and add
      // a group heading. Otherwise, just add the element to the set.
      $element_set = array();
      if (is_array($elem)) {
        $element_set = $elem;
      }
      else {
        $element_set[$option] = $elem;
      }

      $links = array();
      $multiple = !empty($element['#multiple']);

      // If we're in an exposed block, we'll get passed a path to use for the
      // Views results page.
      $path = '';
      if (!empty($element['#bef_path'])) {
        $path = $element['#bef_path'];
      }

      foreach ($element_set as $key => $value) {
        $element_output = '';
        // Custom ID for each link based on the <select>'s original ID.
        $id = drupal_html_id($element['#id'] . '-' . $key);
        $elem = array(
          '#id' => $id,
          '#markup' => '',
          '#type' => 'bef-link',
          '#name' => $id,
        );
        if (array_search($key, $selected_options) === FALSE) {
          $elem['#children'] = l($value, bef_replace_query_string_arg($name, $key, $multiple, FALSE, $path));
          //$output .= theme('form_element', array('element' => $elem));
          $element_output = theme('form_element', array('element' => $elem));

          if ($element['#name'] == 'sort_bef_combine' && !empty($element['#settings']['toggle_links'])) {
            $sort_pair = explode(' ', $key);
            if (count($sort_pair) == 2) {
              // Highlight the link if it is the selected sort_by (can be either
              // asc or desc, it doesn't matter).
              if (strpos($selected_options[0], $sort_pair[0]) === 0) {
                $element_output = str_replace('form-item', 'form-item selected', $element_output);
              }
            }
          }
        }
        else {
          $elem['#children'] = l($value, bef_replace_query_string_arg($name, $key, $multiple, TRUE, $path));
          _form_set_class($elem, array('bef-select-as-links-selected'));
          //$output .= str_replace('form-item', 'form-item selected', theme('form_element', array('element' => $elem)));
          $element_output = str_replace('form-item', 'form-item selected', theme('form_element', array('element' => $elem)));
        }
        $output .= $element_output;

      }
    }
    $properties = array(
      '#description' => isset($element['#bef_description']) ? $element['#bef_description'] : '',
      '#children' => $output,
    );

    $output = '<div class="bef-select-as-links">';
    $output .= theme('form_element', array('element' => $properties));
    if (!empty($element['#value'])) {
      if (is_array($element['#value'])) {
        foreach ($element['#value'] as $value) {
          $output .= '<input type="hidden" name="' . $name . '[]" value="' . $value . '" />';
        }
      }
      else {
        $output .= '<input type="hidden" name="' . $name . '" value="' . $element['#value'] . '" />';
      }
    }
    $output .= '</div>';
  }


  return $output;
}

/**
 * Get nodes attached to term
 *
 * @param $tid
 *   ID of term
 *
 * @param string $field
 *  field used to attach node to term
 *
 * @param boolean $published
 *   Get only published nodes
 *
 * @return array $output
 *   array of attached nodes
 */
function _moyrodnoy_taxonomy_get_nodes ($tid = NULL, $published = FALSE, $field = '') {
  $output = array();
  if (!$tid) {
    return $output;
  }
  else if ($field == '') {
    $nids = taxonomy_select_nodes($tid, FALSE);
    if ($published) {
      $nodes = node_load_multiple($nids, array('status' => 1));
      $nids = array();
      foreach ($nodes as $key => $value) {
        $nids[] = $key;
      }
    }
    $output = $nids;
  }
  else {
    $params = array();
    $query = db_select('node', 'n');
    $query->innerJoin('field_data_' . $field, 'f', 'n.nid = f.entity_id');
    $query->fields('n', array('nid'));
    $query->condition('f.' . $field . '_tid', $tid);
    if ($published) {
      $query->condition('n.status', 1);
    }
    $nids = $query->execute()->fetchCol();
    $output = $nids;
  }

  return $output;
}